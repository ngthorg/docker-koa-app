FROM node:6.3.1

MAINTAINER ngthong <ngthorg@gmail.com>

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

VOLUME ["/usr/src/app"]

CMD ["npm", "start"]
