### Contents
+ What is Docker
+ Why Node.js & Docker are a good pair
+ Docker Basics
+ Build and deploy a Node.js app with Docker

### What is Docker
+ Docker is Lunix Containers...
  + with good UK
  + with a package manager
+ What are LXCs?
  + LXCs are like chroots
  + Can limit slice of OS resources allocated to them
  + Isolation b/w containers liks VMs
  + Faster to start than VMs
  + Direct access to OS resources
+ What Docker does is not new
  + But they nailed the UX for devs
+ Docker consists of:
  + A daemon with an API
  + A CLI
  + Extra components (compose, swarm, machine)


### #1 build
```
$ docker build -t koa-app .
or
$ docker build -t koa-app:lastest .
```

### #2 start app
```
$ docker run -p 8080:8080 -d koa-app
```
or
sync file
```
$ docker run -v $(pwd):/usr/src/app -p 8080:8080 -d koa-app
```


### #* All
```
docker ps
docker ps -a
docker ps -q
```
```
docker stop <container-id>
```
```
docker rmi <container-id>
```
```
docker history koa-app:0.0.1
```
