/**
 * @flow
 */
const Koa = require('koa');
const http = require('http');
const router = require('koa-router')();
const co = require('co');
const convert = require('koa-convert');
const logger = require('koa-logger');

const PORT = process.env.PORT || 80;
const app = new Koa();


router.use(logger());

router.get('/', ctx => {
  ctx.body = 'Hello World!';
});

router.get('*', ctx => {
  ctx.body = '404 NotFound!';
});


app
  .use(router.routes())
  .use(router.allowedMethods());

const server = http.createServer(app.callback());

server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
