/* @flow */
import A from './moduleA';


class Human extends A {
  getInfo() {
    return `name: ${this.getName()}; age: ${this.getAge()}`;
  }

  getTest(): number {
    return this.getAge();
  }
}


const human = new Human('ng thorg', 23);
console.log(human.getInfo());
